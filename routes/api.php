<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\RegistrationController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\EmailVerifyController;
use App\Http\Controllers\Api\ForgetPassword;


Route::group(['middleware' => 'api'],function($routes){
    //================ user registration, login, refresh-token and logout ================//
    Route::post('/register',[RegistrationController::class,'register']);
    Route::post('/login',[LoginController::class,'login']);
    Route::get('/refresh-token',[LoginController::class,'refreshToken']);
    Route::delete('/logout',[LoginController::class,'logout']);

    ///============= forget password ===================///
    Route::post('/forget-password',[ForgetPassword::class,'forgetPassword']);

    //================ verify email ==================//
    Route::get('/send-verify-mail/{email}',[EmailVerifyController::class,'sendVerifyMail']);

    //================== user profile =======================//
    Route::get('/profile',[UserController::class,'profile']);
    Route::put('/profile-update',[UserController::class,'profileUpdate']);
    
});