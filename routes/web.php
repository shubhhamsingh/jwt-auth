<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\EmailVerifyController;
use App\Http\Controllers\Api\ForgetPassword;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//================ Email Verification ===================//
Route::get('/verify-mail/{token}',[EmailVerifyController::class,'verificationMail']);

///================ Reset Password ======================//
Route::get('/reset-password',[ForgetPassword::class,'resetPasswordLoad']);
Route::Post('/reset-password',[ForgetPassword::class,'resetPassword']);