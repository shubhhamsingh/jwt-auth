<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Mail;
use Illuminate\Support\str;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Models\PasswordReset;
use Validator;
use Illuminate\Support\Facades\Hash;
use DB;

class ForgetPassword extends Controller
{
    public function forgetPassword(Request $request)
    {
        try {
            $user = User::where('email',$request->email)->get();
            if(count($user) > 0)
            {
                $token = Str::random(40);
                $domain = URL::to('/');
                $url = $domain."/reset-password?token=".$token;

                $data['url'] = $url;
                $data['email'] = $request->email;
                $data['title'] = 'Pasword Reset';
                $data['body'] = 'Click on below link to reset your password';

                Mail::send('forget-password',['data' => $data], function($message) use ($data){
                    $message->to($data['email'])->subject($data['title']);
                });

                $datetime = Carbon::now()->format('Y-m-d H:i:s');
                PasswordReset::UpdateOrCreate(
                    ['email' => $request->email],
                    [
                        'email' => $request->email,
                        'token' => $token,
                        'created_at' => $datetime
                    ]);
                    
            return response()->json(['message' => 'Please check your mail to reset your password.']);

            }else{
                return response()->json(['message' => 'User not found!']);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }
    }

    //========= reset password view load ==============
    public function resetPasswordLoad(Request $request)
    {
        //$resetData = PasswordReset::where('token',$request->token)->get();

        $resetData = DB::table('password_resets')
        ->where('token',$request->token)
        ->get();
       
        if(isset($request->token) && count($resetData) > 0){
            $user = User::where('email',$resetData[0]->email)->get();
            return view('resetPassword',compact('user'));

        }else{
            return view('404');
        }
    }

    //============ Password reset functionality ==============

    public function resetPassword(Request $request)
    {
        $request->validate([
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::find($request->id);
        $user->password = Hash::make($request->password);
        $user->save();

        PasswordReset::where('email',$user->email)->delete();
        

        return "<h1>Your Password has been reset successfully.</h1>";
    }
}
