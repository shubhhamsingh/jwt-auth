<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Mail;
use Illuminate\Support\str;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Carbon;

class EmailVerifyController extends Controller
{
    public function sendVerifyMail($email)
    {
        if(auth()->user())
        {
            $user = User::where('email',$email)->get();
            if(count($user) > 0){

                $random = Str::random(40);
                $domain = URL::to('/');
                $url = $domain."/verify-mail/".$random;

                $data['url'] = $url;
                $data['email'] = $email;
                $data['title'] = 'Email Verification';
                $data['body'] = 'Please click here to below to verify your mail.';

                Mail::send('verifyMail',['data' => $data], function($message) use ($data){
                    $message->to($data['email'])->subject($data['title']);
                });

                $user = User::find($user[0]['id']);
                $user->remember_token = $random;
                $user->save();

                return response()->json(['message' => 'Mail sent successfully.']);

            }else{
                return response()->json(['message' => 'User not Found!'],404);
            }

        }else{
            return response()->json(['message' => 'User is Not Authenticated.'],401);
        }
    }

    public function verificationMail($token)
    {
        $user = User::where('remember_token',$token)->get();
        if(count($user) > 0)
        {
            $datatime = Carbon::now()->format('Y-m-d H:i:s');
            $user = User::find($user[0]['id']);
            $user->remember_token = '';
            $user->is_verified = 1;
            $user->email_verified_at = $datatime;
            $user->save();
            
            return "<h1>Email Verify Successfully.</h1>";
        }else{
            return view('404');
        }
    }
}
