<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Validator;
use Exception;

class LoginController extends Controller
{
    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email',
            'password' => 'required|string|min:6'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        // !$token = auth()->setTTL(1)->attempt($validator->validate());    #set expire time via override setTTL() method

        if(!$token = auth()->attempt($validator->validate())){
            return response()->json(['error'=>'Unauthorized'],401);
        }

        return $this->responseWithToken($token);
    }

    protected function responseWithToken($token){
        return response()->json([
            'token' => $token,
            'type' => 'Bearer',
            'expires_in' => auth()->factory()->getTTL()*60 // expire in second
        ]);
    }

    public function refreshToken(){
        if(auth()->User()){
            return $this->responseWithToken(auth()->refresh());
        }else{
            return response()->json(['message' => 'User not authenticated.']);
        }
        
    }

    public function logout(){
        try {
            auth()->logout();
            return response()->json(['message'=>'User logout successfully.']);
        } catch (\Exception $e) {
            return response()->json(['message'=> $e->getMessage()]);
        }
        
    }
}
