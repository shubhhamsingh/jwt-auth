<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Exception;


class UserController extends Controller
{

    public function profile(Request $request){
        try {
            return response()->json(['data' => auth::user()]); 
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }
        
    }

    public function profileUpdate(Request $request)
    {
        if(auth()->user())
        {
           $validator =  validator::make($request->all(),[
                'id' => 'required',
                'name' => 'required|string',
                'email' => 'required|email|string|'
            ]);

            if($validator->fails())
            {
                return response()->json($validator->errors());
            }

            $user = User::find($request->id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
            return response()->json(['data' => $user],200);
        }else{

            return response()->json(['message' => 'User is not authenticated.'],401);
        }
    }



}
